package pe.uni.jesusramirezm.ef;


public class Pregunta {

    public String text;
    public Integer imageID;
    public Boolean value;

    public Pregunta(String text, Boolean value, Integer imageID) {
        this.text = text;
        this.imageID = imageID;
        this.value = value;
    }
}
