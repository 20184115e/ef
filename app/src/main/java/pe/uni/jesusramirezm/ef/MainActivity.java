package pe.uni.jesusramirezm.ef;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bosphere.filelogger.FL;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Para los logs
    private static final String TAG = MainActivity.class.getSimpleName();

    TextView textViewPregunta;
    ImageView imageViewPregunta;
    Button buttonBack, buttonNext;
    RelativeLayout relativeLayout;
    Button buttonTrue, buttonFalse;


    ArrayList<Pregunta> listaPreguntas = new ArrayList<>();

    String[] lista_preguntas;
    ArrayList<Integer> lista_imagenes = new ArrayList<>();
    ArrayList<Boolean> lista_valores = new ArrayList<>();

    int posicionActual = 0;
    int totalPreguntas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        FL.i(TAG,"onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPregunta = findViewById(R.id.question);
        imageViewPregunta = findViewById(R.id.image);
        buttonBack = findViewById(R.id.button_back);
        buttonNext = findViewById(R.id.button_next);
        buttonTrue = findViewById(R.id.button_true);
        buttonFalse = findViewById(R.id.button_false);
        relativeLayout = findViewById(R.id.relativeLayout);

        lista_preguntas = getResources().getStringArray(R.array.preguntas);
        totalPreguntas = lista_preguntas.length;

        fillArray();

        for (int i = 0; i < lista_preguntas.length; i++) {

            String preguntaText = lista_preguntas[i];
            listaPreguntas.add(new Pregunta(preguntaText, lista_valores.get(i), lista_imagenes.get(i)));

        }

        textViewPregunta.setText(listaPreguntas.get(posicionActual).text);

        buttonNext.setOnClickListener(v -> {

            FL.i("buttonNext setOnClickListener");

            if (posicionActual + 1 == totalPreguntas) {
                posicionActual = 0;
            }
            else { posicionActual = posicionActual + 1;}
            textViewPregunta.setText(listaPreguntas.get(posicionActual).text);
            imageViewPregunta.setImageResource(listaPreguntas.get(posicionActual).imageID);
        });

        buttonBack.setOnClickListener(v -> {

            FL.i("buttonBack setOnClickListener");

            if (posicionActual != 0) {

                posicionActual = posicionActual - 1;
            }

            textViewPregunta.setText(listaPreguntas.get(posicionActual).text);
            imageViewPregunta.setImageResource(listaPreguntas.get(posicionActual).imageID);
        });

        buttonTrue.setOnClickListener(v -> {

            FL.i("buttonTrue setOnClickListener");

            if (lista_valores.get((posicionActual))) {

                Snackbar.make(relativeLayout, R.string.msg_correcto, Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", v1 -> {

                }).show();
            }

            else {

                Snackbar.make(relativeLayout, R.string.msg_incorrecto, Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", v12 -> {

                }).show();

            }
        });

        buttonFalse.setOnClickListener(v -> {

            FL.i("buttonFalse setOnClickListener");

            if (!lista_valores.get((posicionActual))) {

                Snackbar.make(relativeLayout, R.string.msg_correcto, Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", v13 -> {

                }).show();
            }

            else {

                Snackbar.make(relativeLayout, R.string.msg_incorrecto, Snackbar.LENGTH_INDEFINITE).setAction("Cerrar", v14 -> {

                }).show();

            }
        });



    }


    private void fillArray() {

        lista_valores.add(true);
        lista_valores.add(false);
        lista_valores.add(true);
        lista_valores.add(false);
        lista_valores.add(true);
        lista_valores.add(false);


        lista_imagenes.add(R.drawable.hachiman);
        lista_imagenes.add(R.drawable.half_life);
        lista_imagenes.add(R.drawable.hatsune);
        lista_imagenes.add(R.drawable.sans);
        lista_imagenes.add(R.drawable.twilight);
        lista_imagenes.add(R.drawable.terranigma);

    }

    @Override
    protected void onPause() {
        super.onPause();
        FL.i("onDestroy");
    }


}